.. _species_determination:
Species determination
=====================
.. note::
   You can access the handout for the morphological characters `here`_. And here is the `Weigmann`_. See also the videos under :ref:`lectures`.

.. _here: https://owncloud.gwdg.de/index.php/s/G2uw1WuGphcuBnM
.. _Weigmann: https://owncloud.gwdg.de/index.php/s/yvh2DcvEVPidLpx

