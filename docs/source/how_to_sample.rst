.. _how-to-sample:
How to Sample
=============

Check out the video below to find out how to sample oribatid mites.

.. youtube:: 9RRhksEuEpM

.. hint::
   More videos of the soil animal ecology group can be found on `Youtube`_.

.. _Youtube: https://www.youtube.com/@animalecologygoettingen378
