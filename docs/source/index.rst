Welcome to M.Biodiv.411 Evolutionary Ecology!
=============================================

Here you can find the complete documentation of the course, which can also be found under `StudIP`_.

.. _StudIP: https://studip.uni-goettingen.de/dispatch.php/course/overview?cid=d7b80997f5efda59609a4cf69a04dbf7

Check out the :doc:`schedule` for details about the timeline. 

And here are some important **usage** notes, check out :ref:`usage` for more:

.. note::

   These boxes highlight goals and achievements of the course/week/day. Pay attention to them.

.. important::

   ⚠️ These boxes highlight the **Tasks of the Day** and the **Question(s) of the Day**. You should definitely pay attention to them 😊.

.. attention::

  The 🔓 password for unlocking files will be given in class.

Content
=======
.. toctree::
   introduction
   how_to_sample
   species_determination
   first_week
   second_week
   third_week
   schedule
   section
   lectures
   usage
