.. _schedule:
Schedule
--------
.. list-table:: Evolutionary Ecology
   :widths: 10 10 15 30 25 20
   :header-rows: 1

   * - Week
     - Day
     - Date
     - What
     - Where
     - When
   * - First
     - Mon
     - 06.11.23
     - Welcome, introduction, sort animals I, determine I
     - Kursraum C, Heinrich-Düker-Weg 8
     - 9:00-16:00
   * - First
     - Tue
     - 07.11.23
     - Sort animals II, determine II, complete common taxa list
     - Kursraum C, Heinrich-Düker-Weg 8
     - 9:00-16:00
   * - First
     - Wed
     - 08.11.23
     - Introduction molecular work, safety induction
     - room 1.212, Untere Karspüle 2
     - 9:00-10:00
   * - First
     - Wed
     - 08.11.23
     - DNA extraction and PCR
     - molli lab, Untere Karspüle 2, Animal Ecology
     - 10:00-16:00
   * - First
     - Thu
     - 09.11.23
     - Introduction gel electrophoresis, PCR-purification; team a and b
     - molli lab, Untere Karspüle 2, Animal Ecology
     - Team A: 9:00-13:00, Team B: 12:00-16:00
   * - First
     - Fri
     - 10.11.23
     - Sequencing of PCR products, redo failures – optimistically: free
     - 
     - 
   * - Second
     - Mon
     - 13.11.23
     - Review of the first week, start practical work with Geneious Prime and upload consensus sequences, Lecture & Tutorial 1: how to handle and edit sequence data
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Second
     - Tue
     - 14.11.23
     - Lecture & Tutorial 2: sequence alignment
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Second
     - Wed
     - 15.11.23
     - Lecture & Tutorial 3: models of sequence evolution and cluster methods
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Second
     - Thu
     - 16.11.23
     - Lecture & Tutorial 4: search algorithms & MrBayes
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Second
     - Fri
     - 17.11.23
     - Lecture & Tutorial 5: phylogeny in R
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Third
     - Mon
     - 20.11.22
     - Review the second week, develop and conduct your 🧬mini research project🧬, use own data, build hypothesis, start creating datasets fitting to analyze your research question
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Third
     - Tue
     - 21.11.22
     - Conduct 🧬mini research project🧬
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Third
     - Wed
     - 22.11.22
     - Results of 🧬mini research project🧬, discuss, prepare presentation
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Third
     - Thu
     - 23.11.22
     - Results of 🧬mini research project🧬, discuss, prepare presentation
     - room MN37, Untere Karspüle 2
     - 9:00-16:00
   * - Third
     - Fri
     - 24.11.22
     - Presentation of 🧬mini research project🧬
     - room MN37, Untere Karspüle 2
     - 9:00-...
