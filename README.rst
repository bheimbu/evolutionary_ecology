Welcome to M.Biodiv.411 Evolutionary Ecology
============================================

This GitHub page includes the link to the documentation on `Read the Docs <https://about.readthedocs.com/?ref=readthedocs.com>`_.

See the course material here:

https://mbiodiv411-evolutionary-ecology.readthedocs.io/en/latest/
